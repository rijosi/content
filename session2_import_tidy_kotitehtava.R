#' ---
#' title: "Kotitehtävä 2"
#' author: utu_tunnus
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session2_import_tidy_kotitehtava.R)

#+ setup
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))

#' # Kansiorakenteen luominen
#' 
#' R:ssä on kommennot käyttöjärjestelmän tiedostojärjestelmän käyttöön, kuten tiedostojen luomiseen (`file.create()`) 
#' kansioiden luomiseen (`dir.create()`).
#' 
#' **Kysymys:** *Millä komennolla luot nykyisen työhakemistoon kansion `aineisto`?*
#+ vastaus1
default_answer(1)

#' 
#' # Datatiedoston tallentaminen koneelle
#' 
#' Käyttämämme data löytyy osoitteesta https://vincentarelbundock.github.io/Rdatasets/csv/plm/Males.csv
#' 
#' **Kysymys:** *Miten tallennan ko. tiedoston kansioon `aineisto`?*
#+ vastaus2
default_answer(2)

#' 
#' # Datatiedoston tuominen R:ään
#' 
#' Ohjeistus tähän
#' 
#' **Kysymys:** *Kysymys tähän?*
#+ vastaus3


#' 
#' # Datan siivoaminen R:ssä
#' 
#' Ohjeistus tähän
#' 
#' **Kysymys:** *Kysymys tähän?*
#+ vastaus4


